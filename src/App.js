import Content from "./views/Content";
import Header from "./views/Header";

function App() {
  return (
    <div>
     <Header/>
     <Content />
    </div>
  );
}

export default App;
