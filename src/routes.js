import FirstPage from "./pages/FirstPage";
import HomePage from "./pages/HomePage";
import SecondPage from "./pages/SecondPage";
import ThirdPage from "./pages/ThirdPage";

const routerList = [
    { path: "/", element:  <HomePage /> },
    { label: "FirstPage", path: "/firstpage", element: <FirstPage /> },
    { label: "SecondPage", path: "/secondpage", element: <SecondPage /> },
    { label: "SecondPage with Param", path: "/secondpage/:pageid", href: "/secondpage/12", element: <SecondPage /> },
    { label: "SecondPage with Param Third", href: "/secondpage/third", element: <SecondPage /> },
    { label: "ThirdPage", path: "/thirdpage", element: <ThirdPage /> },
]

export default routerList;