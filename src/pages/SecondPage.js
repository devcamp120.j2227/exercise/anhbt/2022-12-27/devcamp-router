import { useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";

const SecondPage = () => {
    const navigate = useNavigate();

    const { pageid } = useParams();

    useEffect(() => {
        if ( pageid === "third" ) {
            navigate("/thirdpage");
        }
    })

    return (
        <div>
            {
                pageid ?
                "SecondPage with ID: " + pageid
                : " SecondPage"
            }
        </div>
    )
}

export default SecondPage;